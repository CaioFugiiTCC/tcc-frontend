import axios from "axios";

const apiRefundMe = axios.create({
  baseURL: process.env.REACT_APP_API_REFUNDME
});

export default apiRefundMe;
