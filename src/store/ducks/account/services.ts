import apiRefundMe from '../../../services/apiRefundMe';

import { Account } from '../../../models/Account';

export const get = async (token: Account) => {
  try {
    const data  = await apiRefundMe.get(`/users/${token._id}`);
    // console.log(data);
    return data;
  } catch (error) {
    const message = error.response
      ? error.response.data.message
      : error.message;
    throw Error(message);
  }
};
