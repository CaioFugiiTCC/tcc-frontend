
interface BankAccount {
  account: string,
  agency: string,
  bankName: string,
}

interface Owner {
  _id: string;
  name: string;
  email: string;
  bankAccount: BankAccount;
}

interface Users {
  _id:string,
  name:string,
  email:string
}

export interface RefundsModel {
  _id?: string,
  title: string,
  personInCharge: Users,
  owner: Owner
  refunds: string[],
  createdAt?: string,
  status?: string,
  checked?: boolean,
  datePayment?: any
}