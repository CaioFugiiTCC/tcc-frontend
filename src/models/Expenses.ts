interface BankAccount {
  account: string,
  agency: string,
  bankName: string,
}

interface Owner {
  _id?: string;
  name: string;
  email: string;
  bankAccount: BankAccount;
}

export interface ExpensesModel {
  _id?: any;
  date: any;
  classification: string;
  value: number;
  file: any;
  status?: string;
  owner: Owner;
}